use std::collections::BTreeMap;

pub mod general;
pub mod file;
pub mod package;
pub mod user;

#[derive(Clone, Debug, Default, Deserialize)]
pub struct Config {
    // @adig general::prompt false rhega, ie. do not prompt if settings are not defined
    pub general: general::GeneralConfig,

    // @adig Ye PackageName -> {} hai sbme, ie. Default PackageConfig, ie. version, git, path all None 
    #[serde(default)]
    pub packages: BTreeMap<String, package::PackageConfig>,

    // Ye kai files aur unke data ka array hai, jo individually FileConfig me store ho rha h, ek example de sakte hai idhar
    #[serde(default)]
    pub files: Vec<file::FileConfig>,

    // do keys hai `root` aur `user`, aur unka UserConfig, eg. passwords etc.
    #[serde(default)]
    pub users: BTreeMap<String, user::UserConfig>,
}
